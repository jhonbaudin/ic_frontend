## Inilialization

After you run your local server with the [ic_backend](https://gitlab.com/jhonbaudin/ic_backend) then run the following commands to start with the frontend application

```sh
git clone https://gitlab.com/jhonbaudin/ic_frontend.git ic_frontend && cd ic_frontend && npm install
```

After the installation, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!